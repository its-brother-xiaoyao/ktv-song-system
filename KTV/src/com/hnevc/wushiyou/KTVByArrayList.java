package com.hnevc.wushiyou;

import java.util.Scanner;
import java.util.ArrayList;



public class KTVByArrayList {


    public static void main(String[] args) {
        System.out.println("欢迎来到点歌系统");
        System.out.println("0.添加歌曲至列表");
        System.out.println("1.将歌曲顶置");
        System.out.println("2.将歌曲前移一位");
        System.out.println("3.退出");
       ArrayList lineUpList = new ArrayList();//创建歌曲列表
        addMusicList(lineUpList);//添加一部分歌曲至歌曲列表
        while (true) {
            System.out.println("请输入执行的操作序号:");
            Scanner scan = new Scanner(System.in);
            int command = scan.nextInt();//接受键盘苏输入的功能选项序号
            switch (command) {
                case 0:
                    addMusicList(lineUpList);
                    break;//奖歌曲添加至列表
                case 1:
                    setTop(lineUpList);
                    break;//将歌曲顶置
                case 2:
                    setBefore(lineUpList);
                    break;//将歌曲前移一位
                case 3:
                    exit();
                    break;//退出
                default:
                    System.out.println("-------------------------");
                    System.out.println("功能选择有误，请输入正确的编号！");
                    break;
            }
            System.out.println("当前歌曲列表：" + lineUpList);
        }
    }


    //初始时添加歌曲名称
    private static void addMusicList(ArrayList lineUpList) {
        lineUpList.add("Because of you");
        lineUpList.add("love me like you do");
        lineUpList.add("lemon");
        lineUpList.add("晴天");
        lineUpList.add("天梯");
        lineUpList.add("这就是爱情");
        lineUpList.add("我的一个道姑朋友");
        System.out.println("初始歌曲列表:" + lineUpList);

    }

    //执行添加歌曲
    private static void addMusic(ArrayList lineUpList) {
        System.out.println("请输入要添加的歌曲名称:");
        String musicName = new Scanner(System.in).nextLine();
        lineUpList.add(musicName);//将歌曲添加到列表最后
        System.out.println("已添加歌曲：" + musicName);
    }

    //执行将歌曲顶置
    private static void setTop(ArrayList lineUpList) {
        System.out.println("请输入要顶置的歌曲名称：");
        String musicName = new Scanner(System.in).nextLine();
        int position = lineUpList.indexOf(musicName);//查找定位歌曲位置
        if (position < 0) {
            System.out.println("当前列表中没有此歌曲");
        }//判断出入的歌曲是否存在
        else {
            lineUpList.remove(musicName);//移除指定的歌曲
            lineUpList.add(0,musicName);//将指定的歌曲放在第一位
        }
        System.out.println("已将歌曲" + musicName + "顶置");
    }

    //执行将歌曲置前一位
    private static void setBefore(ArrayList lineUpList) {
        System.out.println("请输入要置前歌曲的名称：");
        String musicName = new Scanner(System.in).nextLine();
        int position = lineUpList.indexOf(musicName);//查找指定歌曲的位置
        if (position < 0) {
            System.out.println("当前列表中没有此歌曲");
        }//判断输入歌曲是否存在
        else if (position == 0) {
            System.out.println("当前歌曲已在最顶部！");
        }//判断歌曲是否已在第一位
        else {
            lineUpList.remove(musicName);//移除指定的歌曲
            lineUpList.add(position - 1, musicName);//将指定的歌曲放到前一位
        }
        System.out.println("已将" + musicName + "置前一位");
    }

    //退出
    private static void exit() {
        System.out.println("---------退出---------");
        System.out.println("您已退出系统");
        System.exit(0);
    }

}

